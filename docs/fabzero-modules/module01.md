# 1. Project management and documentation

In order to have proper project management, a version control system is needed. This is where Git comes in. 

Git allows for keeping track of changes made to your project and easy collaboration with others.

Git also offers features such as:
- Branching
- Merging
- Reverting

## 1.1. Basics of Git

### 1.1.1. Cloning a repository
To access a repository stored on a remote server, you need to clone it. 
This basically means that you are creating a local copy of the repository on your machine.

Services storing git repositories such as GitHub and GitLab require you to authenticate before you can clone a repository.
This is done by generating an SSH key and adding it to your account.

**Generating an ssh key on Linux**
Simply run the following command in your terminal and follow instructions:
```bash
ssh-keygen
```

**Copying public ssh key**
In order to copy your public ssh key to your clipboard, run the following command (assuming that you named your key `id_rsa`):
```bash
cat ~/.ssh/id_rsa.pub | xclip -selection clipboard
```

Then just copy this key in the SSH keys section of your account settings on GitHub or GitLab.

Once done, you can use the following command to clone a repo:
```bash
git clone <repository_url>
```

You can then `cd` into the newly created directory and start working on the project.

### 1.1.2. Committing changes
In order to commit changes to your project, you need to add them to the staging area. This is done using the following command:
```bash
git add <file_name>
```

You can also add all files in the current directory using the following command:
```bash
git add .
```

Once you have added all the files you want to commit, you can commit them using the following command:
```bash
git commit -m "<commit_message>"
```

The `-m` flag allows you to specify a commit message. You can also use `-am` to add and commit in one command.

Also bear in mind that commits are not automatically pushed to the remote repository. You need to do this manually.

### 1.1.3. Pushing changes
Once you have committed your changes, you can push them to the remote repository using the following command:
```bash
git push
```

## 1.2. Branching
Git branches are a feature of the Git version control system that allow developers to work on multiple versions 
of a codebase simultaneously. Each branch represents a separate line of development, allowing developers to experiment
with new features or make changes without affecting the main codebase

### 1.2.1. Creating a new branch

In order to create a new branch, use the following command:
```bash
git switch -c <branch_name>
```

The `-c` flag creates a new branch and switches to it.


### 1.2.2. Switching between branches

In order to switch between branches, use the following command:
```bash
git switch <branch_name>
```

Here we do not use the `-c` flag as we are not creating a new branch.

### 1.2. Merging branches
Branches can be merged back into the main codebase when changes are complete, allowing for collaborative development 
and ensuring that changes are properly reviewed and tested before they are integrated into the main codebase.

In order to merge a branch into the main codebase, use the following command:
```bash
git merge <branch_name>
```

You might then be faced with a merge conflict. This basically means that the same lines of code have been changed in
both branches. In order to resolve this, you will have to manually edit the conflicting files. There are some great tools
that can help you with this, such as [Meld](https://meldmerge.org/). If using a JetBrains IDE, you can also use the built-in
merge tool, which works similarly to Meld.

## 1.3. Reverting
Reverting refers to the process of undoing a previous commit or a series of commits, 
essentially rolling back changes made to a codebase. This is useful when a commit has caused issues or introduced bugs,
and it's necessary to revert back to a previous state of the codebase.

To revert a commit in Git, you can use the git revert command followed by the commit hash of the commit you want to revert. Here are the steps to follow:

1. First, navigate to the local Git repository on your machine using a terminal or command prompt.

2. Use the ```git log``` command to view the commit history and find the hash of the commit you want to revert.

3. Once you have the commit hash, use the git revert command followed by the hash. For example:
```bash
git revert abc1234
```
This will create a new commit that undoes the changes made in the specified commit.

If you need to revert multiple commits, you can specify a range of commits using the git revert command. For example:

```bash
git revert abc1234..def5678
```

This will revert all commits in the range abc1234 to def5678, creating a new commit for each revert.


## 1.3. Documentation
Documentation is an important part of any project. 
It allows for easy collaboration and makes it easier for others to understand your project.

### 1.3.1. Markdown
Markdown is a lightweight markup language that allows you to create formatted text using plain text syntax.
It's commonly used for writing documentation, README files, and web content. Here's a quick guide to the basics of Markdown:

**Headings**

To create a heading, use one to six # symbols at the beginning of a line. 
The number of symbols corresponds to the level of the heading (e.g. one symbol for the largest heading, six symbols for the smallest heading).

Example:

```markdown
# Heading 1
## Heading 2
### Heading 3
```

**Bold and Italic**

To make text bold, wrap it in two ** symbols. To make text italic, wrap it in one * symbol.

Example:

```markdown
This is **bold** text.
This is *italic* text.
```

**Strikethrough**

To add a strikethrough effect to text, wrap it in two ~~ symbols.

Example:

```markdown
This is ~~strikethrough~~ text.
```

**Lists**
- Unordered List

To create an unordered list, use the - or * symbol followed by a space at the beginning of a line.

Example:
    
```markdown
- Item 1
- Item 2
- Item 3
```

- Ordered List

To create an ordered list, use a number followed by a . and a space at the beginning of a line.

Example:

```markdown
1. Item 1
2. Item 2
3. Item 3
```

**Links**

To create a link, use the following syntax: `[link text](url)`

Example:


```markdown
Here is a [link](https://www.example.com/) to an example website.
```

**Images**

To include an image, use the following syntax: `![alt text](image url)`

Example:


```markdown
![Example Image](https://www.example.com/image.png)
```

**Code**

To format code inline, wrap it in backticks (`).

Example:


```markdown
Use the `git commit` command to commit changes.
```

To create a code block, indent the code by four spaces or use triple backticks (`) at the beginning and end of the block.

Example:

```python
def greet(name):
    print(f"Hello, {name}!")
```
That's it! These are the basics of Markdown. There are many more features and syntax elements you can use to create richly formatted text. Happy writing!


# 1.4. Image compression

In order to more efficiently store images, it is essential to compress them. This can be done with a multitude of tools,
but here we will focus on [ImageMagick](https://imagemagick.org/index.php).

## 1.4.1 Image Magick

Magick is a free and open-source image manipulation library. It is used to create, edit, compose, or convert bitmap images.
It comes pre-installed on most Linux distributions, and can be installed easily on Windows and macOS as well.

### 1.4.2 Ubuntu installation

To install ImageMagick on Ubuntu, use the following command:

```bash
sudo apt update
sudo apt upgrade
sudo apt install imagemagick
```

### 1.4.3 Usage

**Compression**

To compress an image, use the following command:

```bash
magick convert <input_file> -quality <quality> <output_file>
```

**Conversion**

To convert an image to a different format, use the following command where you simply need to specify the output format:

```bash
magick convert <input_file> <output_file>
```

**Resizing**

To resize an image, use the following command:

```bash
magick convert <input_file> -resize <width>x<height> <output_file>
```

**Cropping**

To crop an image, use the following command:

```bash
magick convert <input_file> -crop <width>x<height>+<x>+<y> <output_file>
```

You can do so much more with ImageMagick, but these are the basics. 
For more information, check out the [documentation](https://imagemagick.org/script/command-line-processing.php).


