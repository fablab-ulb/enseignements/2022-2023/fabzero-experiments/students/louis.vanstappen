# 3. 3D printing

This week I printed the design I made last week.
This process required two steps: slicing the model and printing it.
I used the PrusaSlicer software to slice the model and the Prusa Mini printer to print it.

## 3.1. Extruding the model

Before slicing the model, I had to extrude the sketch. I simply did so in FreeCad with a width of 10cm, which proved to
be too thick.

Once the model was extruded, I exported it as an STL file.

## 3.2. Slicing the model

### 3.2.1. PrusaSlicer

The [PrusaSlicer](https://www.prusa3d.com/prusaslicer/) is a slicing software that is used to prepare the model for
printing. It is available for Windows, Mac and Linux. This software allows for previewing the model, changing the
settings of the printer, and exporting the model as a G-code file. It is also filled with a lot of features that I did
not use as they were not needed for this print.

I used this software to slice the model with the default settings for the Prusa Mini printer.

### 3.2.2. Exporting the model

Once the model was sliced, I exported it as a G-code file and stored it on an SD card in order to load it afterward
on the printer.

## 3.3. Printing

Printing was pretty straight forward. I simply loaded the G-code file on the printer and started the print. 

Here's a cool video of the print:

![print](./images/printing.gif)

However, as the print went on, I realized that I had set an overly thick extrusion width. Hence, I decided to stop the
print. I realized that for this basic print, the width at which I stopped the print was enough. I decided to keep the
print as it was.

## 3.4. Result

Finally, the result was pretty good. The print was a bit rough, but it was good enough for what I needed. I was able to
play with the antenna, and it was robust enough to not break.

Here's the final result:

![3d-print](./images/result.jpg)