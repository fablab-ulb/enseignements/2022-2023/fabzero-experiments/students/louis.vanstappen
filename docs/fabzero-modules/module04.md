# 4. Laser cutter

This week I learned how to use a laser cutter. We learned how to use different laser cutters and ended up
using the homemade version one.

# 4.0.1. Prerequisites

The following tools were required to complete this module:

- Inkscape: an open source svg editor
- Laser cutter software: a software to control the laser cutter (we used the one provided on the computers in the
  Fablab)

# 4.0.2. Safety instructions

The laser cutter is a dangerous tool. It can cause fire and damage to the eyes. It is important to follow the
safety instructions. 

- Do not leave the laser cutter unattended.
- Do not look at the laser while it is cutting unless you are wearing the appropriate glasses.
- Always wear the appropriate glasses when the laser is cutting.
- Do not cut materials that are not allowed as they can release toxic fumes or cause other problems.
- Always check the material you are cutting is not flammable.
- Always make sure the laser cutter is well ventilated by ensuring that the ventilation system is turned on and working properly.
- Preview the cut before starting it to make sure the laser will not cut outside the material.

Each laser cutter may have different safety instructions. Make sure to read them before using the laser cutter.

# 4.1. Group cut

## 4.1.1. Laser cut software

The software is pretty basic and runs on a Raspberry Pi. It is only possible to import SVG files.
Once the files are imported, the only options presented are to decide the power of the laser for each
layer and to set the speed of the laser. The software also allows to set the origin of the laser cutter
and to preview the cut.


# 4.1.2. Cutting

In order to cut a shape, we first needed to create an SVG file using Inkscape. We can then import it into the software and
preview the cut.

We created the circle shape in Inkscape.

**SVG File**

![SVG file](./images/module4/circle.svg)

Then, we set the circle inside a rectangle shape and exported the SVG file.

**Cut**

Before starting the cut we needed to load the material into the laser cutter.

![Loading the material](./images/module4/insert.jpg)

**Cut settings**

We parametrized the laser cutter to cut the rectangle and engrave the circle.

Here are the settings we used:

- Engrave: 50 power, 200 speed
- Cut: 150 power, 75 speed

We had to perform the cutting part twice for the laser cutter to properly cut the shape.

**Cut Preview**

And finally we previewed the cut outline.

![Cut preview](./images/module4/preview-cut.gif)

Once that was done, we started the cut.

# 4.1.3. Result

Despite our best efforts, we managed to laser the laser cutter. No damage was done, but it still complicated
the process.

After fixing the laser cutter, we were able to properly cut the shape.

Here is the result:

![Result](./images/module4/result.jpg)

[//]: # (# 4.2.1. Individual cut)

[//]: # ()
[//]: # (For my individual cut, I decided to attempt to engrave a map of Europe on a piece of plastic.)

[//]: # ()
[//]: # (# 4.2.2. SVG file)

[//]: # ()
[//]: # (I am using the following SVG file:)

[//]: # ()
[//]: # (![Europe svg file]&#40;./images/module4/europe.svg&#41;)

[//]: # ()
[//]: # (# 4.2.3. Cut)

[//]: # ()
[//]: # (I unfortunately did not have time to cut the map, but I will update this section once I do.)

[//]: # ()

