# 5. Group dynamic and final project

## 5.1. Group dynamic

## 5.1.1. Sharing the floor

**Talking stick**

Each person has a chance to speak, and the person holding the stick has the floor. 
The stick is passed to the next person when they are done speaking.

**Time limit**

Each person has a time limit to speak. The time limit is set by the group and someone is
charged with keeping track of the time.

**Speaking turn**

Each person has a turn to speak. The order is set by the group.

**Finger**

Each person can raise their finger to speak. The person with the floor can decide to let
a person with their finger speak or to continue speaking.

# 5.1.2. Individual roles

During each meeting, it is interesting to assign a role to each person.

**Animator**

The animator is in charge of keeping the meeting on track.

**Secretary**

The secretary is in charge of taking notes.

**Timekeeper**

The timekeeper is in charge of keeping track of the time.

# 5.1.3. Decision process

There are multiple ways to take decisions in a group.

**Consensus**

The decision is taken when everyone agrees.

**Majority**

The decision is taken when the majority of the group agrees.

**Weighted votes**

Depending on the value of one's opinion, one can have more or less votes.

**Draw**

If there is a tie, a draw can be used to break the tie.


# 5.2. Group project

## 5.2.1. Chosen object

Before being assigned into groups, we were asked to choose an object that had importance to us. I picked my phone
because it is a clear example of an issue that is poorly addressed by most companies. The phone just like a lot of 
other electronic devices are not designed to be repaired. This problem is not only an environmental issue, but also
a social issue. The lack of repairability of electronic devices is chosen by design in order to force people to buy
new devices. 

## 5.2.2. Chosen group issue

Once the group were formed, we were asked to choose an issue that we wanted to address. We chose to address the
issue of repairability of electronic devices both physically and software wise. We were thinking of a way to reuse old
devices in order to create a new device that could serve a different purpose.

## 5.2.3. Experience gained

During our brainstorming session, we came up with a lot of ideas. It was interesting to use the different techniques
mentioned in the previous section to make decisions on how to orient our ideas as a group. I am glad that we were
able to come up with a lot of ideas and that we were able to narrow them down to a few that we could work on.


# 5.3. Problem tree

In order to create a problem tree, our group started by identifying the basic problem that brought us together,
namely ecology and more specifically E-waste, which represents electronic and electrical waste.
Modern life is defined by electronic and electrical gadgets. It is hard to imagine living without them,
whether it's smartphones, computers, washing machines or vacuum cleaners. 
This wide variety of products are usually thrown away after be used whether they are still functional or not.
However, the waste they produce complicates the efforts to reduce their environmental impact.
It's important to note that less than 40% of e-waste is recycled in the EU, making it one of the fastest growing waste streams.

![Problem tree](./images/problem_tree.png)

# 5.4. Objective tree

We then used this information to create an objective tree.

![Objective tree](./images/objective_tree.png)
