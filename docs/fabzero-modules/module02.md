# 2. Computer Aided Design (CAD)

This week I worked on CAD design. By worked I mean learned because I really did not know anything about it. The only comparable activity I had previously done was some 3D modeling in Blender.

## 2.1. OpenSCAD

OpenSCAD is a free and opensource cad software that enable designing 3D objects using a sort of programming language with a familiar syntax.

It offers methods to generate basic objects and others to accomplish actions such as merging objects. There's also some nice features such as *for loops*.

**TLDR**, use the [cheatsheet](https://openscad.org/cheatsheet/).

### 2.1.1. Objects

Here are some of the basic objects that can be created.

#### Cube
To create a cube, use the cube function which takes three arguments: _width_, _height_, and _depth_.

```openscad
cube([10, 10, 10]);
```

#### Sphere
To create a sphere, use the sphere function which takes one argument: _radius_.

```openscad
sphere(10);
```

#### Cylinder
To create a cylinder, use the cylinder function which takes two arguments: _height_ and _radius_.

```openscad
cylinder(h = 10, r = 10);
```

### 2.1.2. Actions
Here are some basic actions that can be performed on objects.

#### Translate
To translate an object, use the translate function which takes two arguments: _vector_ and _object_.

```openscad
translate([10, 10, 10]) cube([10, 10, 10]);
```

#### Rotate
To rotate an object, use the rotate function which takes two arguments: _vector_ and _object_.

```openscad
rotate([90, 0, 0]) cube([10, 10, 10]);
```

#### Scale
To scale an object, use the scale function which takes two arguments: _vector_ and _object_.

```openscad
scale([2, 2, 2]) cube([10, 10, 10]);
```

#### Union
To merge two objects, use the union function which takes two arguments: _object1_ and _object2_.

```openscad
union() {
    cube([10, 10, 10]);
    translate([10, 10, 10]) cube([10, 10, 10]);
}
```

### 2.1.3. Loops
Here are some basic loops that can be used.

#### For
To create a for loop, use the for function which takes three arguments: _variable_, _start_, and _end_.

```openscad
for (i = [0:10]) {
    translate([i, 0, 0]) cube([10, 10, 10]);
}
```

### 2.1.4. Modules
Modules are a way to create reusable objects. They are similar to _Macros_ found in other programming languages. They are defined using the module function which takes two arguments: _name_ and _object_.

```openscad
module cube() {
    cube([10, 10, 10]);
}
```

which can then be used like this:

```openscad
cube();
```

### 2.1.5. Variables
Variables are a way to store values. They are defined using the let function which takes two arguments: _name_ and _value_.

```openscad
var a = 10;
cube([a, a, a]);
```


### 2.1.6. Why not ?

After trying out to model a Flexilink part, I realized that I did not find it quite intuitive and I wanted something requiring more visual interaction.


# 2.2. FreeCAD

FreeCAD is a free and opensource cad software that enable designing 3D objects using a graphical interface.

I decided to learn FreeCAD by following this [tutorial](https://www.youtube.com/watch?v=1QNVyMBV0fM&t=1302s).

## 2.2.1. Sketches
Sketches are used to create 2D shapes. These 2D shapes can then be used to create 3D objects by _extruding them_.

###  Creating a sketch
To create a sketch, click on the _Sketch_ button in the _Part_ tab.

The sketch used sizing values from a spreadsheet.

![Sketch](./images/3d-sketch.png)


### Extruding a sketch
Once the sketch was created, it was only a matter of extruding it.
I used the _Pad_ tool in the _Part Design_ tab to do so.

![Extrude](./images/3d-extruded.png)


# 2.3. CC Licenses

Whenever content is created and shared publicly online, such as code on Github, the author needs a way to
legally maintain some control over their work.
Creative Commons licenses are a standardised way to share content while keeping some rights on it.
Their main purpose is to state what can and cannot be done with the content.

There are multiple ones that are free and easy to use, such as:

## 2.3.1. CC BY

The CC BY license allows anyone to share and adapt the content as long as the author is credited.

## 2.3.2. CC BY-SA

The CC BY-SA license allows anyone to share and adapt the content as long as the author is credited and the content is shared under the same license.

## 2.3.3. CC BY-NC

The CC BY-NC license allows anyone to share and adapt the content as long as the author is credited and the content is not used for commercial purposes.


[//]: # (## 3D Models)

[//]: # ()
[//]: # (<div class="sketchfab-embed-wrapper"><iframe width="640" height="480" src="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>)

[//]: # ()
[//]: # (<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">)

[//]: # (    <a href="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Dita&#39;s Gown</a>)

[//]: # (    by <a href="https://sketchfab.com/francisbitontistudio?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Francis Bitonti Studio</a>)

[//]: # (    on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a>)

[//]: # (</p>)

[//]: # (</div>)
