## Foreword

Welcome to my amazing doc repo. Here you can find cool documentation written for the
**PHYS-F517 - How To Make (almost) Any Experiment Using Digital Fabrication** course.

This documentation was written over the course of multiple weeks. It covers new concepts and
techniques that were introduced in the course.


## About me

![](images/avatar-photo.jpg)

Hi! My name is Louis Vanstappen. I am a student at the Université Libre de Bruxelles (ULB) in Belgium. I am currently in 
my 3rd year of my bachelor's degree in Computer Science. I chose to follow this course because it seemed quite interesting.

## My background

As my studies may imply, I am quite interested in computers and programming. I hope to be able to apply my skills
in real life applications with the help of this course.

## Previous work

I have some experience with microcontrollers and electronics. I have worked with Arduino and Raspberry Pis in the past.
I really enjoy working with ESP32s as they are very versatile and can be used for many applications.


